<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','api_token','phone','avatar','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function register(Request $request)
    {
        return self::create([
            'email'     => $request->email,
            'name'      => $request->name,
            'phone'  => $request->phone,
            'password'  => bcrypt($request->password),
            'address' => $request->address,
            'api_token' => str_random(40),
        ]);
    }

    /* Fungsi ini di gunakan untuk login user */

    public function login(Request $request)
    {
        $credential = [
            'phone' => $request->phone,
            'password' => $request->password,
        ];

        if(auth()->attempt($credential)){
            return true;
        }else{
            return false;
        }
    }
}
