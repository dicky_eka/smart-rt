<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends ApiController
{
    public function index()
    {
        $events = Event::where('approved',true)->get();
        return $this->respondSuccess($events);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required',
            'start'         => 'required',
            'end'           => 'required',
            'lat'           => 'required',
            'long'          => 'required',
            'description'   => 'required',
        ]);

       $event = Event::create($request->all()+['user_id'=>auth()->user()->id]);
       return $this->respondCreated($event);

    }


}
