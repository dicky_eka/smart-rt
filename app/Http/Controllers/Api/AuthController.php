<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends ApiController
{
    protected $user;

    function __construct()
    {
        $this->user = new User();
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required',
            'password' => 'required|min:6',
        ]);

        if($this->user->login($request)){
            return $this->respondSuccess(auth()->user());
        }else{
            $errors[0] = 'password/email anda salah';
            return $this->respondUnauthorizedError($errors);
        }
    }

    public function register(Request $request)
    {

        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email',
            'phone'     => 'required|unique:users,phone',
            'password'  => 'required|min:6',
            'address' => 'required'
        ]);

        $user = $this->user->register($request);

        return $this->respondCreated($user);
    }



}
